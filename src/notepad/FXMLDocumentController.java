/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notepad;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;

/**
 *
 * @author arashdeep
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    Label label;
    @FXML
    TextArea textarea;
    Stage st = new Stage();
    Stage stage = new Stage();
    Stage stage2 = new Stage();
    FileWriter path = null;

    @FXML
    private void saveAction(ActionEvent event) throws IOException {
        if (label.getText() == "") {
            saveas(st, textarea);
        } else {
            save(st, textarea);
        }
    }

    @FXML
    private void saveAsAction(ActionEvent event) throws IOException {
        saveas(st, textarea);
    }

    @FXML
    private void openAction(ActionEvent event) throws IOException {
        open(st, textarea);
    }

    @FXML
    private void newAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        stage2.setTitle("NotePad");
        stage2.setScene(scene);
        stage2.initModality(Modality.APPLICATION_MODAL);
        stage2.show();
    }

    public void save(Stage st, TextArea textarea) throws IOException {

        String text = textarea.getText();
        FileWriter file = new FileWriter(label.getText());
        BufferedWriter Writer = new BufferedWriter(file);
        Writer.write(text);
        Writer.flush();
        Writer.close();
        file.close();

    }

    public void saveas(Stage st, TextArea textarea) throws IOException {
        String text = textarea.getText();
        FileChooser filechooser = new FileChooser();
        // filechooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CComma Delimited (*.csv)", "*.csv"));
        File file = filechooser.showSaveDialog(st);
        FileWriter fw = new FileWriter(file);
        label.setText(file.getPath());
        BufferedWriter Writer = new BufferedWriter(fw);
        Writer.write(text);
        Writer.flush();
        Writer.close();
    }
    char ch;

    public void open(Stage st, TextArea textarea) throws IOException {
        FileChooser filechooser = new FileChooser();
        // filechooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CComma Delimited (*.csv)", "*.csv"));
        File file = filechooser.showOpenDialog(st);
        FileInputStream fw = new FileInputStream(file);
        label.setText(file.getPath());
        BufferedInputStream buffin = new BufferedInputStream(fw);
        int i;
        byte[] bit = new byte[1024];
        while ((i = buffin.read(bit)) != -1) {
            String txt = new String(bit);
            textarea.setText(txt);

        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
